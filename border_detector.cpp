#include "border_detector.h"

border_detector::border_detector( const cv::Mat& image): m_image( image.clone()) 
														,m_contoured_image()
														,m_contour_fixer()
														,m_bin_result()
{}

void border_detector::launch()
{
	approach* detector;

//	detector = new hayk();/
//	detector->launch( m_image, &m_results);
//	delete detector;

	detector = new cannyFix();
	detector->launch( m_image, &m_results);
	delete detector;
	new_window( m_image, "m_image");
	detector = new cannyFix2();
	detector->launch( m_image, &m_results);
	delete detector;

	detector = new cannyFix3();
	detector->launch(m_image, &m_results);
	delete detector;

	detector = new cannyFix4();
	detector->launch(m_image, &m_results);
	delete detector;

	detector = new cannyFix5();
	detector->launch(m_image, &m_results);
	delete detector;

	detector = new cannyHsv();
	detector->launch( m_image, &m_results);
	delete detector;

	detector = new cannyLab();
	detector->launch( m_image, &m_results);
	delete detector;

	m_contour_fixer.launch( m_results);
	get_final_bin(4);

	draw_contour();


}
cv::Mat border_detector::get_contoured_image() const
{
	return m_contoured_image.clone();
}

cv::Mat border_detector::get_original_image() const 
{
	return m_image.clone();
}

cv::Mat border_detector::get_bin_image() const
{
	return m_bin_result.clone();
}


void border_detector::get_final_bin( int pass_rate) 
{
	pass_rate = m_results.size() - 1;
	m_bin_result = cv::Mat( m_image.size(), CV_8UC1);
	m_bin_result.setTo( 0);
	for(int  i = 0; i < m_image.rows; ++i){
		for(int  j = 0; j < m_image.cols; ++j){
			int count = 0;
			for(int k = 0; k < m_results.size(); ++k){
				if( m_results[k].at<uchar>( i,j) != 0){
					++count;
				}
			}
			if( count >= pass_rate){
				m_bin_result.at< uchar>(i,j) = 255;
			}
		}
	}
}

void border_detector::draw_contour()
{
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};
	m_contoured_image = m_image.clone();

	bool** mark = new bool*[m_image.rows];
	for(int i = 0; i < m_image.rows; ++i){
		mark[i] = new bool[m_image.cols];
		for(int j = 0; j < m_image.cols; ++j){
			mark[i][j] = false; 
		}
	}

	mark[0][0] = true;
	std::queue<cv::Point> q;
	q.push( cv::Point(0,0));

	while( ! q.empty())
	{
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for(int i = 0; i < 8; ++i){
			int newX = x + px[i];
			int newY = y + py[i];
			if( is_valid_coordinate(m_image, newX, newY) && !mark[newX][newY]){
				if (m_bin_result.at< uchar> (newX, newY) != 0){
					m_contoured_image.at< cv::Vec3b>(newX, newY) = pure_green;	
				} else {
					mark[newX][newY] = true;
					q.push( cv::Point( newX,newY));
				}
			}
		}
	}
	for(int i = 0; i < m_image.rows; ++i){
		delete[] mark[i];
	}
	delete[] mark;
}


void border_detector::display_all_results()
{
	for(int  i = 0; i < m_results.size(); ++i){
		cv::Mat temp;
		::draw_contour( m_image, m_results[i], temp );
		new_window( temp, std::to_string(i) + "  th reuslts");

	}
}