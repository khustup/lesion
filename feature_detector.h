#ifndef FEATURE_DETECTOR_INCLUDED
#define FEATURE_DETECTOR_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "table_printer.h"
#include <map>
#include <queue> 
#include "functions.h"
#include <cmath>
const double PI = 2 * acos(0);
class feature_detector
{
public:
	feature_detector( const cv::Mat&, const cv::Mat& );	
	virtual ~feature_detector();
	virtual double get_feature() const = 0;
	cv::Mat m_image;
	cv::Mat m_contoured_image;
	cv::Mat m_bin_image;
	std::vector< cv::Point> m_contour;
	int m_fgPixels_count;
	int m_bgPixels_count;
	std::vector< cv::Point> m_fgPixels;
	std::vector< cv::Point> m_bgPixels;
};

class irregularity_index_detector: public feature_detector
{
public:
	irregularity_index_detector( const cv::Mat&, const cv::Mat&);
	~irregularity_index_detector();
	double get_feature() const override; 

};

class color_variance_detector: public feature_detector
{
public:
	color_variance_detector( const cv::Mat& , const cv::Mat&, int);
	~color_variance_detector();
	double get_feature() const override;
	int m_channel;
};

class color_relative_cromaticity: public feature_detector
{
public:
	color_relative_cromaticity( const cv::Mat&, const cv::Mat&, int);
	~color_relative_cromaticity();
	double get_feature() const override;
	int m_channel;
};

class color_coordinates: public feature_detector
{
public:
	color_coordinates( const cv::Mat&, const cv::Mat& );
	~color_coordinates();
	std::vector< double>  get_features();
	double get_feature() const override;
};

#endif
