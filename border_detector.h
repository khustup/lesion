#ifndef EDGE_DETECTOR_INCLUDED
#define EDGE_DETECTOR_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "table_printer.h"
#include <map>
#include <queue> 
#include "functions.h"
#include "detector_approaches.h"
#include "result_fixer.h"

class border_detector 
{
public:
	border_detector( const cv::Mat&);
	void launch() ;
	cv::Mat get_contoured_image() const;
	cv::Mat get_original_image() const;
	cv::Mat get_bin_image() const;
	void display_all_results();
private:
	void get_final_bin( int );
	void draw_contour();
	std::vector< cv::Mat> m_results;
	cv::Mat m_image;
	cv::Mat m_contoured_image;
	cv::Mat m_bin_result;
	result_fixer m_contour_fixer;

};
#endif