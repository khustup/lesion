#include "functions.h"
void new_window(const cv::Mat& image, const std::string& name)
{
	cv::namedWindow( name, cv::WINDOW_AUTOSIZE);
	cv::destroyAllWindows();
	cv::imshow( name, image);
	cv::waitKey(0);
}


void retina_parvo(const cv::Mat& image, cv::Mat& destination)
{
	cv::Ptr <cv::Retina> retina (new cv::Retina(image.size()));
	retina->clearBuffers();
	retina->run(image);
	retina->getParvo(destination);
}

void channel_division(const cv::Mat& image, cv::Mat& destination, int chan1, int chan2)
{
	cv::Mat res(image.rows, image.cols, CV_8UC3, cv::Scalar( 127,127,127));
	for (int i = 0; i < image.rows; ++i) {
		for(int j = 0; j < image.cols; ++j) {
			cv::Vec3b pixel = image.at< cv::Vec3b > (i,j);
			if (pixel.val[chan2] != 0) {
				float val = static_cast<float> ( pixel.val[chan1]) / static_cast<float> ( pixel.val[chan2]);
				val *= 100;
				int intVal = static_cast<int>(val);
				res.at<cv::Vec3b>(i,j) = cv::Vec3b(intVal, intVal, intVal);
			}
		}
	}
	destination  = res.clone();
}

void otsu_threshold(const cv::Mat& image, cv::Mat& destination)
{
	cv::threshold(image, destination,0,255,CV_THRESH_OTSU);
}

void generate_samples(const cv::Mat& image, cv::Mat& destination)
{
	cv::Mat float_image;
	image.convertTo( float_image, CV_32F);
	cv::Mat res(image.rows * image.cols, 3, CV_32FC1);
	int index = 0;
	for (int  i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			res.at<cv::Vec3f>(index++, 0) = float_image.at<cv::Vec3f>(i,j);
		}
	}

	destination = res.clone();

}

bool is_valid_coordinate( const cv::Mat& image, int x, int y)
{
	if (x < 0 || y < 0 || x >= image.rows || y >= image.cols ) {
		return false;
	}
	return true;
}

void prepare_mask_for_grabcut( const cv::Mat& image, cv::Mat& destination)
{
	cv::Mat mask( image.size(), CV_8UC1);
	mask.setTo(cv::GC_FGD);
	for (int i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			if ( is_white( image, i, j) ) {
				mask.at<uchar>(i,j) = cv::GC_PR_FGD;
			}
		}
	}
	bool ** mark = new bool*[image.rows];
	for (int i = 0; i < image.rows; ++i) {
		mark[i] = new bool[image.cols];
		for (int j = 0; j < image.cols; ++j) {
			mark[i][j] = false;
		}
	}
	std::queue<cv::Point> q;
	q.push(cv::Point( 0,0));
	mark[0][0] = true;
	int px[8] = {0,0,-1,1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};
	while (!q.empty()) {
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for (int i = 0; i < 8; ++i) {
			int newX = x + px[i];
			int newY = y + py[i];
			if (is_valid_coordinate( image, newX, newY) && !mark[newX][newY] && is_black(image, newX, newY)) {
				mark[newX][newY] = true;
				q.push( cv::Point( newX, newY));
				mask.at<uchar> ( newX, newY) = cv::GC_BGD;
			}
		}
	}
	destination = mask.clone();
}

bool is_white(const cv::Mat& image, int x, int y)
{
	if (image.at<cv::Vec3b>(x,y) == cv::Vec3b(255,255,255)) {
		return true;
	}
	return false;
}

bool is_black(const cv::Mat& image, int x, int y)
{
	if (image.at<cv::Vec3b> (x,y) == cv::Vec3b( 0,0,0) ) {
		return true;
	}
	return false;
}

void get_bin_mask( const cv::Mat& mask, cv::Mat& binMask)
{
	if (binMask.empty() || binMask.rows != mask.rows || binMask.cols != mask.cols)
        binMask.create( mask.size(), CV_8UC1 );
    binMask = mask & 1;
}

void prepare_samples_for_svm( const cv::Mat& image, const cv::Mat& original_mask, cv::Mat& trainingData, cv::Mat& labels)
{
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};

	trainingData.release();
	labels.release();
	int totalSamples = image.cols * image.rows;
	for (int i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			if ( is_white( image, i,j)) {
				--totalSamples;
			}
		}
	}
	trainingData = cv::Mat( totalSamples, 3, CV_32F);
	labels = cv::Mat( totalSamples, 1, CV_32F);
	cv::Mat mask = original_mask.clone();

	bool** mark = new bool*[image.rows];

	for (int  i = 0; i < image.rows; ++i) {
		mark[i] = new bool[image.cols];
	}
	std::queue<cv::Point> q;
	mark[0][0] = true;
	q.push( cv::Point( 0,0));
	for (int i = 0; i < 3; ++i) {
		trainingData.at<float>(0, i) = image.at< cv::Vec3b>(0,0).val[i];
	}
	labels.at<float>(0,0) = 1;
	
	int curSample = 1;
	while (! q.empty())	{
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for (int i = 0; i < 8; ++i) {
			int newX = x + px[i];
			int newY = y + py[i];
			if (is_valid_coordinate( image, newX, newY) && !is_white(mask, newX, newY) ) {
				mask.at<cv::Vec3b>(newX, newY) = pure_white;
				q.push( cv::Point( newX, newY));
				for(int k = 0; k < 3; ++k){
					trainingData.at< float> ( curSample, k) = static_cast<float>(image.at< cv::Vec3b> ( newX, newY).val[k]);		
				}
				labels.at<float> (curSample, 1) = 1;
				++curSample;
			}
		}
	}
	for (int i = 0; i < image.rows; ++i) {
		for (int  j = 0; j < image.cols; ++j) {
			if (!is_white( mask, i, j)) {
				for (int k = 0; k <3; ++k) {
					trainingData.at< float> (curSample, k) = static_cast<float>(image.at< cv::Vec3b>( i,j).val[k]);
				}
				labels.at<float> (curSample, 0) = 2;
					++curSample;
			
			}
		}
	}
	for (int i = 0; i < image.rows; ++i) {
		delete[] mark[i];
	}
	delete[] mark;
}

void prepare_mask_for_grabcut_pixel_check(const cv::Mat& edges, const cv::Mat& thick_edges, cv::Mat& mask)
{
	cv::Mat result(edges.size(), CV_8UC3);
	mask = cv::Mat(edges.size(), CV_8UC1);
	mask.setTo(cv::GC_BGD);
	for (int i = 0; i < edges.rows; ++i) {
		for (int  j = 0; j < edges.cols; ++j) {
			if(is_white(edges, i, j)){
				mask.at<uchar>(i,j) = cv::GC_FGD;
			}
		}
	}
	for (int i = 0; i < edges.rows; ++i) {
		for (int j = 0; j < edges.cols; ++j) {
			if (is_white( thick_edges, i, j) && mask.at<uchar>(i,j) == cv::GC_BGD) {
				mask.at<uchar>( i,j)  = cv::GC_PR_FGD;
			}
		}
	}
	for (int  i = 0; i < edges.rows; ++i) {
	 	for (int  j = 0; j < edges.cols; ++j) {
			if(!is_white( edges, i, j)) {
				int white_count = get_white_count_in_all_directions(edges, i, j);
				if( white_count >= 6) {
					result.at< cv::Vec3b> (i,j ) = pure_green;
					mask.at< uchar>(i,j) = cv::GC_FGD;
				}
			}
		}
	}
}
int get_white_count_in_all_directions( const cv::Mat& image, int x, int y)
{
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};
	int res = 0;
	for (int i = 0; i < 8; ++i) {
		if (white_exists_in_current_direction(image, x, y, px[i], py[i])) {
			++res;
		}
	}
	return res;
}	

bool white_exists_in_current_direction( const cv::Mat& image, int x,int y,int xDir, int yDir)
{
	if (!is_valid_coordinate( image, x, y)) {
		return false;
	}
	if (image.at<uchar>(x,y) == 255) {
		return true;
	}
	return white_exists_in_current_direction(image, x + xDir, y + yDir, xDir, yDir);
}

void medianCut(const cv::Mat& image, cv::Mat& destination, std::vector< cv::Vec3b>& colorSet)
{
	destination = image.clone();
	for (int i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			int index = 0;
			int dist = color_distance (image.at< cv::Vec3b> (i,j) , colorSet[0]);
			for(int  k = 0; k < colorSet.size();++k) {
				int curDist = color_distance( image.at< cv::Vec3b>(i,j) , colorSet[k]);
				if (curDist < dist) {
					dist = curDist;
					index = k;
				}
			}
			destination.at< cv::Vec3b> (i,j) = colorSet[index];
		}
	}
}

int color_distance(const cv::Vec3b& color1, const cv::Vec3b& color2)
{
	int res = 0;
	for(int  i = 0; i < 3;++i){
		int newVal = static_cast<int> (color1.val[i]) - static_cast<int>(color2.val[i]);
		newVal *= newVal;
		res += newVal;
	}
	return res;
}

float apply_kernel_on_pixel( const cv::Mat& image, int x, int y, const cv::Mat& ker, int size)
{
	float res = 0;
	for (int  i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			res += static_cast<float>(image.at<uchar>( x - (size / 2) + i, y - (size / 2) + j))  * ker.at<float>(i,j);
		}
	}
	return res;
}

void dilate_erode(const cv::Mat& image, cv::Mat& destination, int dilateCount, int erodeCount)
{
	cv::Mat temp;
	cv::dilate(image, temp, cv::Mat(), cv::Point( -1,-1), dilateCount);
	cv::erode(temp, destination, cv::Mat(), cv::Point( -1,-1), erodeCount);
}

void prepare_mask_for_grabcut_contour(const cv::Mat& image, cv::Mat& destination)
{
	destination = cv::Mat( image.size(), CV_8UC1);
	cv::Mat test(image.size(), CV_8UC1);
	test.setTo( 0);
	destination.setTo( cv::GC_BGD);
	for (int i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			if (image.at<uchar> (i,j) == 255) {
				destination.at<uchar>(i,j) = cv::GC_PR_FGD;
			} else {
				int count = get_white_count_in_all_directions( image, i, j);
				if (count >= 7 ) {
					destination.at< uchar> (i,j) = cv::GC_FGD;
					test.at<uchar> (i,j) = 255;
				} else if (count >=5) {
					destination.at< uchar> (i,j) = cv::GC_PR_FGD;
				}
			}
		}
	}
}

void initiate_grabcut( const cv::Mat& image, const cv::Mat& edges, cv::Mat& destination)
{
	cv::Mat mask;
	prepare_mask_for_grabcut_contour( edges,  mask);
	cv::Mat fgdModel, bgdModel;
	cv::grabCut( image, mask, cv::Rect(), bgdModel, fgdModel, 3, cv::GC_INIT_WITH_MASK );
	cv::Mat binMask;
	get_bin_mask( mask, binMask);
	cv::Mat result_cut;
	image.copyTo( result_cut, binMask);
	destination = result_cut.clone();
}

void get_bin_from_color( const cv::Mat& image, cv::Mat& destination)
{
	destination = cv::Mat( image.size(), CV_8UC1);
	destination.setTo( 0);
	for (int i = 0; i < image.rows; ++i) {
		for (int j = 0; j < image.cols; ++j) {
			if (!is_black( image, i, j)) {
				destination.at<uchar>(i,j) = 255;
			}
		}
	}
}

void get_color_from_bin( const cv::Mat& image, const cv::Mat& binImage, cv::Mat& destination)
{
	destination = cv::Mat( image.size(), CV_8UC3);
	destination.setTo( pure_black);
	for( int i = 0; i < image.rows; ++i) {
		for( int  j = 0; j < image.cols; ++j) {
			if( binImage.at<uchar>(i,j) != 0) {
				destination.at< cv::Vec3b> ( i,j) = image.at< cv::Vec3b>(i,j);
			}
		}
	}
}

bool is_on_edge(const cv::Mat& image, int x, int y)
{
	if(x == 0 || y == 0 || x == image.rows - 1 || y == image.cols - 1){
		return true;
	}
	return false;
}

void get_final_bin(const std::vector< cv::Mat>& results, cv::Mat& final_result, int pass_rate)
{
	int rows = results[0].rows;
	int cols = results[0].cols;
	final_result = cv::Mat( rows, cols, CV_8UC1 );
	final_result.setTo( 0);
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			int count = 0;
			for (int k = 0; k < results.size(); ++k) {
				if (results[k].at<uchar>( i,j) != 0) {
					++count;
				}
			}
			if (count >= pass_rate) {
				final_result.at< uchar>(i,j) = 255;
			}
		}
	}
}

void draw_contour( const cv::Mat& image, const cv::Mat& binImage, cv::Mat& destination)
{
	int px[8] = {0,0,1,-1,1,-1,1,-1};
	int py[8] = {1,-1,0,0,1,-1,-1,1};
	destination = image.clone();

	bool** mark = new bool*[image.rows];
	for (int i = 0; i < image.rows; ++i) {
		mark[i] = new bool[image.cols];
		for (int j = 0; j < image.cols; ++j) {
			mark[i][j] = false; 
		}
	}

	mark[0][0] = true;
	std::queue<cv::Point> q;
	q.push(cv::Point(0,0));

	while (!q.empty()) {
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for (int i = 0; i < 8; ++i) {
			int newX = x + px[i];
			int newY = y + py[i];
			if (is_valid_coordinate(image, newX, newY) && !mark[newX][newY]) {
				if (binImage.at< uchar> (newX, newY) != 0) {
					destination.at< cv::Vec3b>(newX, newY) = pure_green;	
				} else {
					mark[newX][newY] = true;
					q.push( cv::Point( newX,newY));
				}
			}
		}
	}
	for (int i = 0; i < image.rows; ++i) {
		delete[] mark[i];
	}
	delete[] mark;
}

void remove_bad_samples( std::vector< cv::Mat>& results)
{
	
}

int white_area( const cv::Mat& image)
{
	int res = 0;
	for (int i = 0; i < image.rows; ++i) {
		for (int  j = 0; j < image.cols; ++j) {
			if (image.at<uchar>(i,j) != 0) {
				++res;
			}
		}
	}
	return res;
}

