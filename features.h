#ifndef FEATURES_INCLUDED
#define FEATURES_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "functions.h"
#include <map>
#include <queue> 
#include <cmath>
class features
{
public:
	const float PI = acos(0) * 2;

	features(const cv::Mat&, const cv::Mat&);
	std::map<std::string, float> detect();

private:
	//preparations 
	void get_bg_and_fg_points();  
	void get_contour_points();
	void calculate_average_colors();	
	//features

	void irregularity_index();
	void asymmetry_index();
	void color_variance();
	void relative_chromaticity();
	void tumor_skin_color_ration();
	void LCHC_difference();
	void diameter_in_pixels();
	void diameter_score();

	std::map<std::string, float> m_features;
	cv::Mat m_bin_image;
	cv::Mat m_color_image;
	std::vector<cv::Point> m_contour_points;
	std::vector<cv::Point> m_bg_points;
	std::vector<cv::Point> m_fg_points;
	float m_average_red_tumor;
	float m_average_green_tumor;
	float m_average_blue_tumor;
	float m_average_red_skin;
	float m_average_green_skin;
	float m_average_blue_skin;

};

#endif