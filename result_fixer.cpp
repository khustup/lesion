#include "result_fixer.h"

void result_fixer::launch( std::vector< cv::Mat>& results) const 
{
	
//	remove_empty_results( results);
}

void result_fixer::fix_border( cv::Mat& image) const
{
	for(int  i = 0; i < image.rows; ++i){
		for(int j = 0; j < 3; ++j){
			if( image.at<uchar> (i,j) == 255 || image.at<uchar>(i, image.cols - 1 - j ) == 255){
				image.setTo(0);
				return;
			}
			flood_fill(image, i,j);
			flood_fill(image, i, image.cols - 1 - j);
		}
	}
	for(int i = 0; i <image.cols; ++i){
		for(int j = 0; j < 3; ++j){
			if( image.at<uchar> (j,i) == 255 || image.at<uchar>(image.rows - 1 - j, i) == 255){
				image.setTo(0);
				return;
			}
			flood_fill( image, j, i );
			flood_fill( image, image.rows - j - j, i);

		}
	}
}

void result_fixer::flood_fill( cv::Mat& image, int x, int y) const
{
	int px[8] = { 0,0,-1,1,-1,1,-1,1};
	int py[8] = { 1,-1,0,0,-1,1,1,-1};
	if( image.at< uchar> (x,y) != 255){
		return;
	}
	std::queue< cv::Point> q;
	q.push( cv::Point(x,y));
	image.at< uchar> (x,y) = 0;
	while( !q.empty())
	{
		int curX = q.front().x;
		int curY = q.front().y;
		q.pop();
		for(int i = 0; i < 8; ++i){
			int newX = curX + px[i];
			int newY = curY + py[i];
			if( is_valid_coordinate(image, newX, newY) && image.at<uchar>(newX,newY) == 255){
				q.push( cv::Point(newX, newY));
				image.at<uchar> (newX,newY) = 0;
			}
		}
	}
}

void result_fixer::remove_empty_results( std::vector< cv::Mat>& results) const
{
	std::vector< cv::Mat> filtered_results;
	for(int i = 0; i < results.size(); ++i){
		if( !is_empty(results[i])){
			filtered_results.push_back(results[i]);
		}
	}
	results = filtered_results;
}

bool result_fixer::is_empty( const cv::Mat& image) const
{
	for(int i = 0; i < image.rows; ++i){
		for(int  j = 0; j < image.cols; ++j){
			if( image.at<uchar> (i,j) == 255){
				return false;
			}
		}
	}
	return true;
}

void result_fixer::leave_biggest_segment( cv::Mat& image) const 
{
	int** grid = new int*[image.rows];
	for(int i = 0; i < image.rows; ++i){
		grid[i] = new int[image.cols];
		for(int j = 0; j < image.cols; ++j){
			grid[i][j] = 0;
		}
	}
	int total_components = 0;
	int biggest_component_index = 0;
	int biggest_component_area = 0;
	cv::Mat temp = image.clone();
	int px[8] = { 0,0,1,-1,1,-1,1,-1};
	int py[8] = { 1,-1,0,0,1,-1,-1,1};
	for(int  i = 0; i < image.rows; ++i){
		for(int  j = 0; j < image.cols; ++j){
			if( temp.at<uchar>(i,j) == 255){
				++total_components;
				int area = 1;
				std::queue< cv::Point > q;
				q.push( cv::Point(i,j) );
				temp.at<uchar>(i,j) = 0;
				grid[i][j] = total_components;
				while( !q.empty()){
					int x = q.front().x;
					int y = q.front().y;
					q.pop();
					for(int k = 0; k < 8; ++k){
						int newX = x + px[k];
						int newY = y + py[k];
						if( is_valid_coordinate(image, newX, newY) && temp.at<uchar>(newX, newY) == 255){
							temp.at<uchar>(newX, newY) = 0;
							q.push( cv::Point(newX,newY) );
							++area;
							grid[newX][newY] = total_components;
						}
					}
				}
				if( area > biggest_component_area){
					biggest_component_area = area;
					biggest_component_index = total_components;
				}
			}
			
		}
	}

	for(int  i = 0; i < image.rows; ++i){
		for(int j = 0; j < image.cols; ++j){
			if( grid[i][j] != biggest_component_index){
				image.at<uchar>(i,j) = 0;
			}
		}
	}

	for(int i = 0; i < image.rows; ++i){
		delete[] grid[i];
	}
	delete[] grid;
}

void result_fixer::drop_multiple_segments( std::vector< cv::Mat >& results) const
{
	std::vector< cv::Mat> final_results;
	for(int  i = 0; i < results.size(); ++i){
		std::cout<< number_of_segments( results[i])<<"  -  ";
		std::cout<< white_area( results[i]) <<"  ";
		if( number_of_segments( results[i]) == 1){
			final_results.push_back( results[i]);
		}
	}
	results = final_results;
}

int result_fixer::number_of_segments( const cv::Mat& image) const
{
	bool** mark = new bool*[image.rows];
	for(int  i = 0; i < image.rows; ++i){
		mark[i] = new bool[image.cols];
		for(int  j = 0; j < image.cols; ++j){
			mark[i][j] = false;
		}
	}
	int px[8] = {0,0,-1,1,-1,1,-1,1};
	int py[8] = {-1,1,0,0,-1,1,1,-1};
	int total = 0;
	for(int  i = 0; i < image.rows; ++i){
		for(int j = 0; j < image.cols; ++j){
			if( !mark[i][j] && image.at<uchar>(i,j) == 255){
				++total;
				mark[i][j] = true;
				std::queue<cv::Point> q;
				q.push( cv::Point(i,j) );
				while(!q.empty()){
					int x = q.front().x;
					int y = q.front().y;
					q.pop();
					for(int k = 0; k < 8; ++k){
						int newX = x + px[i];
						int newY = y + py[i];
						if( is_valid_coordinate( image, newX,newY) && !mark[newX][newY] && image.at<uchar>(newX, newY) == 255){
							mark[newX][newY] = true;
							q.push( cv::Point( newX, newY));
						}
					}
				}
			}
		}
	}
	for(int  i = 0; i < image.rows; ++i){
		delete[] mark[i];
	}
	delete[] mark;
	return total;
}