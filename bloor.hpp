/*
 * bloor.hpp
 *
 *  Created on: Aug 2, 2015
 *      Author: gagikk
 */

#ifndef BLOOR_HPP_
#define BLOOR_HPP_

#include <opencv2/imgproc.hpp>
using namespace cv;

struct Bloor
{
    virtual void
    blur (Mat& res) const
    {
	res = src.clone ();
    }
    ;
    void
    set_src (const Mat& src)
    {
	this->src = src;
    }
    ;
    virtual
    ~Bloor ()
    {
    }
    ;
protected:
    Mat src;
};

struct BlurGauss : public Bloor
{
    BlurGauss (Size ksize = Size (3, 3), double sigmaX = 0, double sigmaY = 0)
	    : ksize (ksize), sigmaX (0), sigmaY (0)
    {
    }
    ;

    void
    blur (Mat& res) const override
    {
	GaussianBlur (src, res, ksize, sigmaX, sigmaY, BORDER_DEFAULT);
    }
private:
    Size ksize;
    double sigmaX, sigmaY;
};

struct BlurMean : public Bloor
{
    BlurMean (Size ksize = Size (3, 3), Point anchor = Point (-1, -1))
	    : ksize (ksize), anchor (anchor)
    {
    }
    ;

    void
    blur (Mat& res) const override
    {
	cv::blur (src, res, ksize, anchor, BORDER_DEFAULT);
    }
private:
    Size ksize;
    Point anchor;
};

struct BlurMedian : public Bloor
{
    BlurMedian (int ksize = 1)
	    : ksize (ksize)
    {
    }
    ;

    void
    blur (Mat& res) const override
    {
	cv::medianBlur (src, res, ksize);
    }
private:
    int ksize;
};

struct BlurBilateral : public Bloor
{
    BlurBilateral (int diameter = 15, double sigmaColor = 80,
		   double sigmaSpace = 80)
	    : diameter (diameter), sigmaColor (sigmaColor), sigmaSpace (
		    sigmaSpace)
    {
    }
    ;

    void
    blur (Mat& res) const override
    {
	cv::bilateralFilter (src, res, diameter, sigmaColor, sigmaSpace,
			     BORDER_DEFAULT);
    }
private:
    int diameter;
    double sigmaColor, sigmaSpace;
};

struct BlurPoster : public Bloor
{
    BlurPoster (int attempts = 8, int clusts = 6, double eps = .001)
	    : attempts (attempts), clusts (clusts), eps (eps)
    {
    }
    ;

    void
    blur (Mat& res) const override
    {
	Mat src;
	cvtColor (this->src, src, COLOR_BGR2Lab);
	int origRows = src.rows;
	Mat colVec[3];
	split(src, colVec);

	Mat colVec_1color = colVec[1].reshape (1, src.rows * src.cols);

	Mat colVecD, bestLabels, centers;
	// convert to floating point
	colVec_1color.convertTo (colVecD, CV_32F, 1.0 / 255.0);
	kmeans (colVecD, clusts, bestLabels,
		TermCriteria (TermCriteria::MAX_ITER + TermCriteria::EPS,
			      attempts, eps),
		attempts, KMEANS_PP_CENTERS, centers);

	// single channel image of labels
	res = bestLabels.reshape (1, origRows);
	res.convertTo (res, CV_8U);
	normalize (res, res, 0, 255, NORM_MINMAX, CV_8U);
	//cvtColor (res, res, COLOR_GRAY2BGR);
//res = bestLabels;
    }
private:
    int attempts;
    int clusts;
    double eps;
};

#endif /* BLOOR_HPP_ */
