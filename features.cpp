#include "features.h"

features::features( const cv::Mat& color_image, const cv::Mat& bin_image): m_color_image( color_image.clone() )
																		 , m_bin_image( bin_image.clone() )
{
	get_bg_and_fg_points();
	get_contour_points();
	calculate_average_colors();
}

std::map<std::string, float> features::detect()
{
	irregularity_index();
	asymmetry_index();
	color_variance();
	relative_chromaticity();
	tumor_skin_color_ration();
	diameter_in_pixels();
	diameter_score();
	return m_features;
}

void features::get_bg_and_fg_points()
{
	for (int i = 0; i < m_bin_image.rows; ++i) { 
		for (int j = 0; j < m_bin_image.cols; ++j) {
			if (m_bin_image.at<uchar>(i,j) == 0) {
				m_bg_points.push_back(cv::Point(i,j));
			} else {
				m_fg_points.push_back(cv::Point(i,j));
			}
		}
	}
}

void features::get_contour_points() 
{
	m_contour_points.clear();
	bool** mark = new bool*[m_bin_image.rows];
	for (int i = 0; i < m_bin_image.rows; ++i) {
		mark[i] = new bool[m_bin_image.cols];
		for (int j = 0; j < m_bin_image.cols; ++j) {
			mark[i][j] = false;
		}
	}
	int px[8] = {0,0,-1,1,-1,1,-1,1};
	int py[8] = {-1,1,0,0,-1,1,1,-1};
	mark[0][0] = true;
	std::queue<cv::Point> q;
	q.push(cv::Point(0,0));
	while (!q.empty()) {
		int x = q.front().x;
		int y = q.front().y;
		q.pop();
		for(int i = 0; i < 8; ++i) {
			int newX = x + px[i];
			int newY = y + py[i];
			if (is_valid_coordinate(m_bin_image, newX, newY) && ! mark[newX][newY]) {
				mark[newX][newY] = true;
				if (m_bin_image.at<uchar>(newX,newY) == 255) {
					m_contour_points.push_back( cv::Point(newX,newY) );
				} else {
					q.push( cv::Point(newX, newY));
				}
			}
		}
	}
	for (int i = 0 ; i < m_bin_image.rows; ++i) {
		delete[] mark[i];
	}
	delete[] mark;
}

void features::calculate_average_colors()
{
	m_average_red_tumor = m_average_green_tumor = m_average_blue_tumor = m_average_red_skin = m_average_green_skin = m_average_blue_skin = 0;
	for (int i = 0; i < m_bg_points.size(); ++i) {
		m_average_red_skin += static_cast<float>(m_color_image.at< cv::Vec3b >(m_bg_points[i].x, m_bg_points[i].y).val[2]);
		m_average_green_skin += static_cast<float>(m_color_image.at< cv::Vec3b >( m_bg_points[i].x, m_bg_points[i].y).val[1]);
		m_average_blue_skin += static_cast<float>( m_color_image.at< cv::Vec3b >( m_bg_points[i].x, m_bg_points[i].y).val[0]);
	}
	for (int i = 0; i < m_fg_points.size(); ++i) {
		m_average_red_tumor += static_cast<float>(m_color_image.at< cv::Vec3b >( m_fg_points[i].x, m_fg_points[i].y).val[2]);
		m_average_green_tumor += static_cast<float>(m_color_image.at< cv::Vec3b >( m_fg_points[i].x, m_fg_points[i].y).val[1]);
		m_average_blue_tumor += static_cast<float>(m_color_image.at<cv::Vec3b >( m_fg_points[i].x, m_fg_points[i].y).val[0]);
	}
	m_average_red_tumor /= m_fg_points.size();
	m_average_green_tumor /= m_fg_points.size();
	m_average_blue_tumor /= m_fg_points.size();

	m_average_red_skin /= m_bg_points.size();
	m_average_green_skin /= m_bg_points.size();
	m_average_blue_skin /= m_bg_points.size();
}

//features 

void features::irregularity_index()
{
	float perimeter = m_contour_points.size();
	float area = m_fg_points.size();
	float irregularity_index = (perimeter * perimeter)  / ( 4 * PI * area);
	m_features["Irregularity index"]  = irregularity_index;
}

void features::asymmetry_index()
{
	float contour_area = m_fg_points.size();
	cv::Point2f circle_center;
	float circle_radius;
	cv::minEnclosingCircle( m_contour_points, circle_center, circle_radius);
	float circle_area = circle_radius * circle_radius * PI;
	float asymmetry_index =  1 - contour_area / circle_area;
	m_features["Asymmetry index"] = asymmetry_index;
}

void features::color_variance()
{
	float variance_red = 0, variance_green  = 0, variance_blue = 0;
	for (int i = 0; i < m_fg_points.size(); ++i) {
		int x = m_fg_points[i].x;
		int y = m_fg_points[i].y;
		float redVariance = m_color_image.at<cv::Vec3b>(x, y).val[2];
		float greenVariance = m_color_image.at<cv::Vec3b>(x, y).val[1];
		float blueVariance = m_color_image.at<cv::Vec3b>(x, y).val[0];
		variance_red += (redVariance - m_average_red_tumor) * (redVariance - m_average_red_tumor);
		variance_green += (greenVariance - m_average_green_tumor) * (greenVariance - m_average_green_tumor);
		variance_blue += (blueVariance - m_average_blue_tumor) * (blueVariance - m_average_blue_tumor);
	}
	variance_red = std::sqrt(variance_red);
	variance_blue = std::sqrt( variance_blue);
	variance_green = std::sqrt(variance_green);

	variance_red /= m_fg_points.size();
	variance_green /= m_fg_points.size();
	variance_blue /= m_fg_points.size();
	m_features["Red Variance"] = variance_red;
	m_features["Blue Variance"] = variance_blue;
	m_features["Green variance"] = variance_green;
}

void features::relative_chromaticity()
{
	float skin_sum = m_average_red_skin + m_average_blue_skin + m_average_green_skin;
	float tumor_sum = m_average_blue_tumor + m_average_green_tumor + m_average_red_tumor;

	float rel_chroma_red = m_average_red_tumor / tumor_sum - m_average_red_skin / skin_sum;
	float rel_chroma_green = m_average_green_tumor / tumor_sum - m_average_green_skin / skin_sum; 
	float rel_chrome_blue = m_average_blue_tumor / tumor_sum - m_average_blue_skin / skin_sum;

	m_features["Red Relative Chromaticity"] = rel_chroma_red;
	m_features["Green Relative Chromaticity"] = rel_chroma_green;
	m_features["Blue Relative Chormaticity"] = rel_chrome_blue;

}

void features::tumor_skin_color_ration()
{
	float red_ratio = m_average_red_tumor / m_average_red_skin;
	float green_ratio = m_average_green_tumor / m_average_green_skin;
	float blue_ratio = m_average_blue_tumor / m_average_blue_skin;
	m_features["Tumor / Skin Red Ratio"] = red_ratio;
	m_features["Tumor / Skin Green Ratio"] = green_ratio;
	m_features["Tumor / Skin Blue Ratio"] = blue_ratio;
}

void features::LCHC_difference()
{
	float X0 = 0.95047;
	float Y0 = 1.00000;
	float Z0 = 1.08883;

	float XTumor = 2.7690 * m_average_red_tumor + 1.7518 * m_average_green_tumor + 1.1300 * m_average_blue_tumor;
	float YTumor = 1.0000 * m_average_red_tumor + 5.5907 * m_average_green_tumor + 0.0601 * m_average_blue_tumor;
	float ZTumor = 0.0000 * m_average_red_tumor + 0.0565 * m_average_green_tumor + 5.5943 * m_average_blue_tumor;

	float XSkin = 2.7690 * m_average_red_skin + 1.7518 * m_average_green_skin + 1.1300 * m_average_blue_skin;
	float YSkin = 1.0000 * m_average_red_skin + 5.5907 * m_average_green_skin + 0.0601 * m_average_blue_skin;
	float ZSkin = 0.0000 * m_average_red_skin + 0.0565 * m_average_green_skin + 5.5943 * m_average_blue_skin;

	float one_third = 1.0 / 3.0;
	float xSkinPow = std::pow( XSkin / X0, one_third );
	float ySkinPow = std::pow( YSkin / Y0, one_third );
	float zSkinPow = std::pow( ZSkin / Z0, one_third );
	float xTumorPow = std::pow( XTumor / X0, one_third );
	float yTumorPow = std::pow( YTumor / Y0, one_third );
	float zTumorPow = std::pow( ZTumor / Z0, one_third );

	float LSkin = 116 * ySkinPow - 16;
	float aSkin = 500 * ( xSkinPow - ySkinPow);
	float bSkin = 500 * ( ySkinPow - zSkinPow);
	
	float LTumor = 116 * yTumorPow - 16;
	float aTumor = 500 * ( xTumorPow - yTumorPow);
	float bTumor = 500 * ( yTumorPow - zTumorPow);

}

void features::diameter_in_pixels()
{
	cv::Point2f center;
	float radius;
	cv::minEnclosingCircle( m_contour_points, center, radius);
	m_features["diameter in pixels"] = static_cast<float>( static_cast<int>(radius * 2));
}

void features::diameter_score()
{
	//   is going to be redfeinde later
	if (m_features["diameter in pixels"] > 300) {
		m_features["diameter score"] = 1;
	} else {
		m_features["diameter score"] = 0;
	}
}