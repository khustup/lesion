#ifndef FEATURE_EXTRACTOR_INCLUDED
#define FEATURE_EXTRACTOR_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "functions.h"
#include <map>
#include <queue> 
#include <cmath>
const double PI = std::acos(0) * 2;
class feature_extractor
{
public:
	feature_extractor( const cv::Mat&);
	virtual void launch() = 0;
	virtual std::string description() const = 0;
	cv::Mat m_image;
	std::map< std::string, double> m_features;
	virtual ~feature_extractor();
};

class size_feature_extractor: public feature_extractor
{
public:
	size_feature_extractor( const cv::Mat&);
	~size_feature_extractor();

	void launch() override;
	std::string description() const override;

	std::map< std::string, double> get_extracted_features() const;
	cv::Point get_mass_center() const;
	std::vector< cv::Point> m_contour;

private:
	cv::Point m_mass_center;
	void get_contour_points();
	double extract_irregularity_index() const;
	double extract_asymmetry_index() const;
	int get_contour_area() const;
	int get_contour_perimeter() const;


};


#endif

