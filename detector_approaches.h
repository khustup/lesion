#ifndef DETECTOR_INCLUDED
#define DETECTOR_INCLUDED

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include "functions.h"
#include <string>
#include <vector>

class approach 
{
public:
	virtual std::string description() const = 0;
	virtual void launch( const cv::Mat& , std::vector< cv::Mat>*  = nullptr) const = 0;
	virtual ~approach()
	{
		cv::destroyAllWindows();
	}
};

class cannyFix : public approach
{
public:
	std::string description() const override
	{
		return " initial approach, does some basic filtering and then canny, and then tries to grabcut ";
	}

	void launch( const cv::Mat& original_image, std::vector< cv::Mat> * results = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		//////new_window( image, "image");
		cv::Mat filtered;

		cv::bilateralFilter( image, filtered, 15,80,80);
		cv::Mat eroded;
		dilate_erode( filtered, eroded, 1, 1);
		cv::Mat eroded_gray;
		cvtColor(eroded,  eroded_gray, CV_RGB2GRAY);
		
		cv::Mat edges =  eroded_gray.clone();
		cv::Canny( edges, edges, 30, 40,3);
		////new_window( edges);
		
		cv::Mat tempEdges = edges.clone();
		
		cv::Mat result_cut;
		//new_window( eroded, "eroded");
		//new_window( edges, "edges");
		initiate_grabcut( image, tempEdges, result_cut);
		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);
		new_window( bin_result, "bin results");
		results->push_back( bin_result);
	}

};

class cannyFix2 : public approach
{
public:
	std::string description() const override
	{
		return "  ";
	}
	void launch( const cv::Mat& original_image,  std::vector< cv::Mat>* results = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		cv::Mat filtered;
		cv::bilateralFilter( image, filtered, 15,80,80);
		
		cv::Mat eroded;
		dilate_erode( filtered, eroded, 5, 5);
		
		cv::Mat eroded_gray;
		cvtColor(eroded,  eroded_gray, CV_RGB2GRAY);
		cv::Mat edges =  eroded_gray.clone();
		cv::Canny( edges, edges, 30, 40,3);
		////new_window( edges);
		cv::Mat tempEdges = edges.clone();
		cv::Mat result_cut;
		initiate_grabcut( image, tempEdges, result_cut);
		//////new_window( result_cut, "grabcut res");
		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);
				//new_window( edges, "edges");

		//////new_window( bin_result, "bin");
		if( results != nullptr){
			results->push_back( bin_result);
		}
	}
};	

class cannyFix3: public approach
{
public:
	std::string description( ) const override
	{
		return " similar to the previous methods, in this method we are also using retina parvo channel";
	}
	void launch( const cv::Mat& original_image, std::vector< cv::Mat>* results = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		cv::Mat parvo;
		retina_parvo( image, parvo);
		cv::Mat filtered;
		cv::bilateralFilter( parvo, filtered, 15,80,80);
		cv::Mat eroded;
		dilate_erode( filtered, eroded, 2, 2);
		cv::Mat eroded_gray;
		cvtColor(eroded,  eroded_gray, CV_RGB2GRAY);
		cv::Mat edges =  eroded_gray.clone();
		cv::Canny( edges, edges, 30, 40,3);
		////new_window( edges);
		cv::Mat tempEdges = edges.clone();
		cv::Mat result_cut;
		initiate_grabcut( image, edges, result_cut);
		//////new_window( result_cut, "result_cut");
		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);
				//new_window( edges, "edges");

		//////new_window( bin_result, "bin");
		if( results != nullptr){
			results->push_back( bin_result);
		}
	}
};

class cannyFix4 : public approach
{
public:
	std::string description() const override
	{
		return " another canny approach, here median Cut algorthm is applied in order to reduce the total number of colors in the image";
	}
	void launch( const cv::Mat& original_image, std::vector< cv::Mat>* results = nullptr) const override 
	{
		cv::Mat image = original_image.clone();
		cv::Mat step1 ;
		dilate_erode( image, step1,   3, 3);
		cv::Mat step2;
		cv::bilateralFilter( step1, step2, 5, 80, 80);
		std::vector< cv::Vec3b> colorSet;

		//dark colors
		for(int i = 0; i < 1; ++i){
			for(int  j = 0; j < 3; ++j){
				for(int k = 0; k < 3; ++k){
					colorSet.push_back( cv::Vec3b ( i * 10, j * 10, k * 10));
				}
			}
		}

		//light colors ( )
		for(int i = 0; i < 3; ++i){
			for(int j = 0; j < 3; ++j){
				for(int k = 0; k < 3; ++k){
					colorSet.push_back( cv::Vec3b( i * 80,  j * 80, j * 80));
				}
			}
		}
		cv::Mat medianCutRes;
		medianCut( step2, medianCutRes, colorSet);
		cv::Mat edges;
		cv::cvtColor( medianCutRes, edges, CV_RGB2GRAY);

		cv::Canny(  edges, edges, 30, 50, 3);
		////new_window( edges);
				//new_window( edges, "edges");

		cv::Mat tempEdges = edges.clone();
		cv::Mat result_cut;
		initiate_grabcut( image, tempEdges, result_cut);
		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);

		if( results != nullptr){
			results->push_back( bin_result);
		}
	}
};

class cannyFix5: public approach
{
public:
	std::string description() const override
	{
		return " this is basically the same approach except it's done in mainly HSV color space" ; 
	}
	void launch( const cv::Mat& original_image, std::vector< cv::Mat>* results = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		cv::Mat hsvImage;
		cv::cvtColor( image, hsvImage, CV_BGR2HSV);
		cv::Mat eroded, dilated; 
		dilate_erode( hsvImage, eroded, 1, 1);		

		cv::Mat eroded_gray(image.size(), CV_8UC1);

		for(int  i = 0;  i < image.rows; ++i){
			for(int j = 0; j < image.cols; ++j){
				eroded_gray.at<uchar>(i,j) = eroded.at<cv::Vec3b>(i,j).val[2];
			}
		}
		cv::Mat edges =  eroded_gray.clone();
		cv::Canny( edges, edges, 50, 60,3);
				//new_window( edges, "edges");

		////new_window( edges);
		
		cv::Mat result_cut, bin_result;
		initiate_grabcut( image, edges, result_cut);
		get_bin_from_color( result_cut, bin_result);
		if( results != nullptr){
			results->push_back( bin_result);
		}	
	}
};

class cannyHsv: public approach
{
	std::string description() const override
	{
		return "another canny approach, the detector is appluied on the hue component in HSv color space after the image being filetered";
	}
	void launch( const cv::Mat& original_image,  std::vector< cv::Mat> * results  = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		cv::Mat filtered;
		cv::bilateralFilter( image, filtered, 15,80,80);
		cv::Mat eroded;
		dilate_erode( filtered, eroded, 2, 2);
		cv::Mat hsv;
		cv::cvtColor( eroded, hsv, CV_RGB2HSV);
		std::vector< cv::Mat> channels;
		cv::split( hsv, channels);
		cv::Mat edges = channels[2].clone();
		////new_window( channels[2], "hue channel");
		cv::Canny( edges, edges, 30, 40, 3);
		////new_window( edges, "new edges");
		cv::Mat tempEdges = edges.clone();
		
		cv::Mat result_cut;
		initiate_grabcut( image, tempEdges, result_cut);

		////new_window( result_cut);

		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);
		////new_window( bin_result, "bin");
		if( results != nullptr){
			results->push_back( bin_result);
		}
	}
};

class cannyLab:public approach
{
	std::string description() const override
	{
		return "same canny approach except this itm ein LAB color space";
	}
	void launch( const cv::Mat& original_image, std::vector< cv::Mat>* results = nullptr) const override
	{
		cv::Mat image = original_image.clone();
		cv::Mat filtered;
		cv::bilateralFilter( image, filtered, 15,80,80);
		cv::Mat eroded;
		dilate_erode( filtered, eroded, 2, 2);
		cv::Mat lab;
		cv::cvtColor( eroded, lab, CV_RGB2Lab);
		std::vector< cv::Mat> channels;
		cv::split( lab, channels);
		for(int  i = 0; i < 3; ++i){
			////new_window( channels[i], "channel " + std::to_string( i));
		}

		cv::Mat edges =  channels[0].clone();
		cv::Canny( edges, edges, 25, 30,3);
		////new_window( edges);
		
		cv::Mat tempEdges = edges.clone();
		
		cv::Mat result_cut;
		initiate_grabcut( image, tempEdges, result_cut);

		////new_window( result_cut);

		cv::Mat bin_result;
		get_bin_from_color( result_cut, bin_result);
		////new_window( bin_result, "bin");
		if( results != nullptr){
			results->push_back( bin_result);
		}

	}

};

class hayk : public approach
{
	public:
	std::string description() const override
	{
		return " initial approach, does some basic filtering and then canny, and then tries to grabcut ";
	}

	void launch( const cv::Mat& original_image, std::vector< cv::Mat> * results = nullptr) const override
	{
		//new_window( original_image, "original_image");
		cv::Mat image = original_image.clone();
		//////new_window( image, "image");
		cv::Mat filtered;

		cv::bilateralFilter( image, filtered, 15,80,80);
		

		
		for(int  i = 0; i < filtered.rows; ++i){
			for(int  j = 0; j < filtered.cols; ++j){
				if (resp_mole_color(filtered.at<cv::Vec3b>(i,j))){
	//				filtered.at< cv::Vec3b> (i,j) = pure_white;
					filtered.at<cv::Vec3b>(i,j).val[2] = 255;
				} else {
	//				filtered.at<cv::Vec3b>(i,j) = pure_black;
			//		filtered.at<cv::Vec3b>(i,j).val[2] = 0;
				}
			}

	}
	//new_window( filtered, "Hayk");
	int avG = 0, avB = 0;
	for(int  i = 0; i < filtered.rows; ++i){
		for(int  j = 0; j < filtered.cols; ++j){
			avG += static_cast<int>(filtered.at<cv::Vec3b>(i,j).val[1]);
			avB += static_cast<int>(filtered.at<cv::Vec3b>(i,j).val[0]);
		}
	}
	avG /= filtered.rows * filtered.cols;
	avB /= filtered.cols * filtered.rows;

	for(int i = 0; i < filtered.rows; ++i){
		for(int  j = 0; j < filtered.cols; ++j){
			filtered.at<cv::Vec3b>(i,j).val[0] = avB;
			filtered.at<cv::Vec3b>(i,j).val[1] = avG;
		}
	}
	cv::Mat eroded_gray;
	std::vector< cv::Mat> channels;
	cv::split( filtered, channels);

	cv::Mat edges =  eroded_gray.clone();
	eroded_gray = channels[2].clone();
	//new_window( eroded_gray, "red gray");
	cv::Canny( eroded_gray, edges, 10, 30,3);
	

	cv::Mat tempEdges = edges.clone();
	
	cv::Mat result_cut;
	//new_window( edges, "edges");
	initiate_grabcut( image, tempEdges, result_cut);
	//////new_window( result_cut);
	cv::Mat bin_result;
	get_bin_from_color( result_cut, bin_result);
	//////new_window( bin_result, "bin");
	results->push_back( bin_result);
}

    bool resp_mole_color(cv::Vec3b &color) const 
    {
        bool result = false;
        int r = static_cast<int>( color.val[2]);
        int g = static_cast<int>( color.val[1]);
        int b = static_cast<int>( color.val[0]);
        //
        const int thresh = 35;

        // Define the belonging of color to mole.
        int val = g>b? g:b;


        if ( r - val > thresh) {
                result = true;
        }
        //
        return result;
    }

};
class mixedThresholding: public approach
{
	std::string description() const override
	{
		return " this approach is pure thresholding on different values and taking the fusion of all resilts  ( if they are valid)";
	}
	void launch(const cv::Mat& original_image, std::vector<cv::Mat>* results = nullptr ) const override
	{
		cv::Mat hsvImage;
		cv::Mat image;


		cv::cvtColor(original_image, hsvImage, CV_BGR2HSV);
		std::vector<cv::Mat> rgb;
		std::vector<cv::Mat> hsv;
		cv::split(original_image, rgb);
		cv::split(hsvImage, hsv);
		rgb.push_back(hsv[0]);
		rgb.push_back(hsv[1]);
		rgb.push_back(hsv[2]);
		for (int i =  0; i < rgb.size(); ++i) {
			cv::Mat res;
			cv::threshold(rgb[i], res, 0, 255, CV_THRESH_BINARY	| CV_THRESH_OTSU);
			new_window( res, std::to_string(i) + "  th results of thresholding");
		}
		cv::Mat hayk;
		
for(int i = 0; i < hsvImage.rows; ++i){
			for(int  j = 0; j < hsvImage.cols; ++j){
				hsvImage.at<cv::Vec3b>(i,j).val[1] = static_cast<uchar>(1);
			}
		}	
		cv::inRange( hsvImage, cv::Scalar( 0, 0, 60), cv::Scalar( 20, 255, 255),hayk);
		std::vector< cv::Mat > chan;
	//	cv::split( hayk, chan);


	//	chan[1] = ( cv::Mat::zeros(chan[1].rows, chan[1].cols, chan[1].type()));
		

	//	cv::merge(chan, hayk);
		new_window( hayk, "Hayk");
		cv::Mat edges;
		cv::Canny( hayk, edges, 30,60,3);
		cv::Mat tempEdges = edges.clone();
	
		cv::Mat result_cut;
	//new_window( edges, "edges");
		initiate_grabcut( original_image, tempEdges, result_cut);
		new_window( result_cut);
		cv::Mat bin_result;

		get_bin_from_color( result_cut, bin_result);

	}
};
#endif
