#ifndef CONTOUR_FIXER_INCLUDED
#define CONTOUR_FIXER_INCLUDED


#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/opencv.hpp"
#include <string>
#include <vector>
#include <fstream>
#include "table_printer.h"
#include <map>
#include <queue> 

namespace fixer
{
	void erosion_dilation_fix( const cv::Mat& , cv::Mat&, int = 1, int = 1 );
}
#endif